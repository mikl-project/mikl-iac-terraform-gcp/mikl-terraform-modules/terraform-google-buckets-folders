variable "bucket-name" {
type = string
}

variable "location" {
type = string
}

variable "storage-class" {
type = string
}

variable "versioning" {
type = bool
}

variable "force-destroy" {
type = bool
}

variable "folders" {
type = list(string)
}

variable "depends_on" {
    
}

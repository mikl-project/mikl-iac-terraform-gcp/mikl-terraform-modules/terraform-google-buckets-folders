# Buckets Creation 

resource "google_storage_bucket" "gcp-buckets" {
  name          = var.bucket-name
  location      = var.location
  storage_class = var.storage-class
  versioning {
          enabled = var.versioning
      }
  force_destroy = var.force-destroy
  depends_on = [var.depends-on]
}


#folders/objects creation

resource "google_storage_bucket_object" "bucket-folders" {
    # bucket = "${google_storage_bucket.gcp-buckets.name}"
      bucket = google_storage_bucket.gcp-buckets.name    
      for_each = toset(var.folders)
      name = each.key
      content = "mirkl-buckets-folders"
}
